package com.sheryians.major.service;

import com.sheryians.major.dto.UserRegistrationDTO;
import com.sheryians.major.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {
    User save(UserRegistrationDTO registrationDTO);
}

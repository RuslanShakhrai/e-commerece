package com.sheryians.major.service;

import com.sheryians.major.model.Role;
import com.sheryians.major.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
    private final
    RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    public Optional<Role> getRoleById(int id){
        return roleRepository.findById(id);
    }
}

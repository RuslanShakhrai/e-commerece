package com.sheryians.major.controller;

import com.sheryians.major.global.GlobalData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class LoginController {
    @GetMapping("/login")
    public String showLoginForm(){
        GlobalData.cart.clear();
        return "login";
    }
}

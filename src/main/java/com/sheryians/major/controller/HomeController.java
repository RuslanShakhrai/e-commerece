package com.sheryians.major.controller;

import com.sheryians.major.global.GlobalData;
import com.sheryians.major.service.CategoryService;
import com.sheryians.major.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HomeController {
    private final
    CategoryService categoryService;
    private final
    ProductService productService;
    public HomeController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping({"/","/home"})
    public String homePage(Model model){
        model.addAttribute("cartCount", GlobalData.cart.size());
        return "index";

    }
    @GetMapping("/shop")
    public String shopPage(Model model){
        model.addAttribute("cartCount",GlobalData.cart.size());
        model.addAttribute("categories",categoryService.getAllCategories());
        model.addAttribute("products",productService.getAllProduct());
        return "shop";
    }
    @GetMapping("/shop/category/{id}")
    public String shopByCategory( @PathVariable int id, Model model){
        model.addAttribute("cartCount",GlobalData.cart.size());
        model.addAttribute("products",productService.getAllProductsByCategoryId(id));
        return "shop";
    }
    @GetMapping("/shop/viewproduct/{id}")
    public String viewProduct(@PathVariable Long id, Model model){
        model.addAttribute("cartCount",GlobalData.cart.size());
        model.addAttribute("product",productService.getProductById(id).get());
        return "viewProduct";
    }
}

package com.sheryians.major.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WishController {
    @GetMapping("/wishlist")
    public String showWishlist(){
        return "wishlist";
    }
}

package com.sheryians.major.controller;

import com.sheryians.major.dto.UserRegistrationDTO;
import com.sheryians.major.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/register")
public class RegistrationController {
    private final
    UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }
    @ModelAttribute("user")
    public UserRegistrationDTO userRegistrationDTO(){
        return new UserRegistrationDTO();
    }
    @GetMapping
    public String showRegistrationForm(){
        return "register";
    }
    @PostMapping
    public String addUser(@ModelAttribute("user") UserRegistrationDTO userRegistrationDTO, HttpServletRequest httpServletRequest) throws ServletException {
        userService.save(userRegistrationDTO);
        httpServletRequest.login(userRegistrationDTO.getEmail(),userRegistrationDTO.getPassword());
        return "redirect:/";
    }
}
